package ictgradschool.industry.designpatterns.ex01;

import java.util.ArrayList;
import java.util.List;

public class NestingShape extends Shape {

    List<Shape> shape = new ArrayList<Shape>();


    public NestingShape() {
        super();
    }

    public NestingShape(int x, int y) {
        super(x, y);
    }

    public NestingShape(int x, int y, int deltaX, int deltaY) {
        super(x, y, deltaX, deltaY);
    }

    public NestingShape(int x, int y, int deltaX, int deltaY, int width, int
            height) {
        super(x, y, deltaX, deltaY, width, height);
    }

    public void move(int width, int height) {
        super.move(width, height);
        for (Shape childShape : shape) {
            childShape.move(fWidth, fHeight);
        }
    }

    public void paint(Painter painter) {
        painter.drawRect(fX, fY, fWidth, fHeight);
        painter.translate(fX, fY);
        for (Shape childShape : shape) {
            childShape.paint(painter);
        }
        painter.translate(-fX, -fY);
    }

    public void add(Shape child) throws IllegalArgumentException {

        if (child.parent() != null) {
            throw new IllegalArgumentException("Cannot be added because it already exists.");
        } else if (child.fX + child.fWidth > fWidth || child.fY + child.fHeight > fHeight) {
            throw new IllegalArgumentException("Will not fit within the bounds of NestingShape object.");
        } else if (child.fX < 0 || child.fY < 0) {
            throw new IllegalArgumentException("Will not fit within the bounds of NestingShape object.");
        } else {
            shape.add(child);
            child.setParent(this);
        }

    }

    public void remove(Shape child) {
        shape.remove(child);
        child.setParent(null);
    }

    public Shape shapeAt(int index) throws IndexOutOfBoundsException {
        if (index >= shape.size() || index < 0) {
            throw new IndexOutOfBoundsException("Index out of bounds.");
        }
        return shape.get(index);
    }

    public int shapeCount() {
        return shape.size();
    }

    public int indexOf(Shape child) {
        return shape.indexOf(child);
    }

    public boolean contains(Shape child) {
        return shape.contains(child);
    }


}
