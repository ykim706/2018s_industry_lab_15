package ictgradschool.industry.designpatterns.ex02;

import ictgradschool.industry.designpatterns.ex01.NestingShape;
import ictgradschool.industry.designpatterns.ex01.Shape;

import javax.swing.event.TreeModelListener;
import javax.swing.tree.TreeModel;
import javax.swing.tree.TreePath;
import java.util.Iterator;

public class NestingShapeAdapter implements TreeModel {
    private Shape root;

    public NestingShapeAdapter(Shape root) {
        this.root = root;
    }

    @Override
    public Object getRoot() {
        return root;
    }

    @Override
    public Object getChild(Object parent, int index) {
        Object result = null;
        if (parent instanceof NestingShape) {
            NestingShape nestingShape = (NestingShape) parent;
            result = nestingShape.shapeAt(index);
        }
        return result;

    }

    @Override
    public int getChildCount(Object parent) {
        int result = 0;
        Shape shape = (Shape) parent;

        if (shape instanceof NestingShape) {
            NestingShape nestingShape = (NestingShape) shape;
            result = nestingShape.shapeCount();
        }
        return result;
    }

    @Override
    public boolean isLeaf(Object node) {
        return !(node instanceof NestingShape);
    }

    @Override
    public void valueForPathChanged(TreePath path, Object newValue) {

    } //not needed

    @Override
    public int getIndexOfChild(Object parent, Object child) {
        int indexOfChild = -1;
        if (parent instanceof NestingShape) {
            NestingShape nestingShape = (NestingShape) parent;
            return nestingShape.indexOf((Shape) child);
        } else {
            return indexOfChild;
        }

    }

    @Override
    public void addTreeModelListener(TreeModelListener l) {

    } //not needed

    @Override
    public void removeTreeModelListener(TreeModelListener l) {

    } // not needed
}
